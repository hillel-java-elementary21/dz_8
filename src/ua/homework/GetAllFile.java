package ua.homework;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class GetAllFile {
    private final List<File> list = new ArrayList<>();
    private final String path;

    public GetAllFile(String path) {
        this.path = path;
    }

    public List<File> getList() {
        return list;
    }

    public void findFile(String path) {
        File currentFolder = new File(path);
        if (currentFolder.exists()) {
            for (File f : currentFolder.listFiles()) {
                if (f.isFile()) {
                    list.add(f);
                } else {
                    findFile(f.getPath());
                }
            }
        } else System.out.println("Указанный путь не существует!");
    }

    public void writePath() {
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream("FileList.txt"))) {
            for (File f : list) {
                outputStream.write(f.getAbsolutePath().getBytes(StandardCharsets.UTF_8));
                outputStream.write(0x0A);
            }
            outputStream.flush();
        } catch (IOException exception) {
        }
    }
}
